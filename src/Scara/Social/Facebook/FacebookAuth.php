<?php

namespace Scara\Social\Facebook;

use Facebook\Facebook;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookClient;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;

/**
 * Simple auth class for Facebook
 */
class FacebookAuth
{
    /**
     * Facebook class instance.
     *
     * @var \Facebook\Facebook
     */
    private $_facebook;

    /**
     * Facebook helper.
     *
     * @var \Facebook\Helpers\FacebookRedirectLoginHelper
     */
    private $_helper;

    /**
     * OAuth object.
     *
     * @var \Facebook\Authentication\OAuth2Client
     */
    private $_oauth;

    /**
     * Class constructor.
     *
     * @return void
     */
    public function __construct($options = [])
    {
        $this->_facebook = new Facebook($options);
    }

    /**
     * Get an instance of FacebookAuth.
     *
     * @return \Scara\Social\Facebook\FacebookAuth
     */
    public function getClass()
    {
        return $this;
    }

    /**
     * Gets Facebook's redirect login helper.
     *
     * @return \Facebook\Helpers\FacebookRedirectLoginHelper
     */
    public function getLoginHelper()
    {
        return $this->_facebook->getRedirectLoginHelper();
    }

    /**
     * Gets the login URL.
     *
     * @param string $url
     * @param array  $scope
     *
     * @return string
     */
    public function getLoginUrl($url, $scope = [])
    {
        $h = $this->getLoginHelper();
        return htmlspecialchars($h->getLoginUrl($url, $scope));
    }

    /**
     * Gets the user's access token.
     *
     * @return string
     */
    public function getAccessToken()
    {
        $h = $this->getLoginHelper();

        try {
            $accessToken = $h->getAccessToken();
        } catch(FacebookResponseException $ex) {
            throw new \Exception($ex->getMessage());
            exit;
        } catch(FacebookSDKException $ex) {
            throw new \Exception($ex->getMessage());
            exit;
        }

        if (!isset($accessToken)) {
            if ($h->getError()) {
                $exception = "Error: {$h->getError()}\n";
                $exception .= "Error Code: {$h->getErrorCode()}\n";
                $exception .= "Error Reason: {$h->getErrorReason()}\n";
                $exception .= "Error Description: {$h->getErrorDescription()}\n";
                throw new \Exception("Facebook Error\n\n" . $exception);
            } else {
                throw new \Exception("Bad Request!");
            }
            exit;
        }

        return $this->authenticate($accessToken);
    }

    /**
     * Attempts to get long lived access token.
     *
     * @param string $accessToken
     *
     * @return string
     */
    private function authenticate($accessToken)
    {
        $this->_oauth = $this->_facebook->getOAuth2Client();

        if (!$accessToken->isLongLived()) {
            try {
                $accessToken = $this->_oauth->getLongLivedAccessToken($accessToken);
            } catch (FacebookSDKException $ex) {
                throw new \Exception($ex->getMessage());
                exit;
            }
        }

        return $accessToken->getValue();
    }

    /**
     * Performs a GET request to Facebook.
     *
     * @param string $endpoint
     * @param string $token
     * @param array  $fields
     * @param array  $params
     *
     * @return \Facebook\FacebookResponse
     */
    public function get($endpoint, $token, $fields = [], $params = [])
    {
        $items = '';
        if (!empty($fields)) {
            $items = '?fields=';
            foreach ($fields as $field) {
                $items .= $field.',';
            }
        }

        if (!empty($items)) {
            $endpoint = $endpoint.rtrim($items, ',');
        }

        return $this->request('GET', $endpoint, $token, $params);
    }

    /**
     * Performs a POST request to Facebook.
     *
     * @param string $endpoint
     * @param string $token
     * @param array  $params
     *
     * @return \Facebook\FacebookResponse
     */
    public function post($endpoint, $token, $params = [])
    {
        return $this->request('POST', $endpoint, $token, $params);
    }

    /**
     * Returns the result as JSON.
     *
     * @param \Facebook\FacebookResponse $response
     *
     * @return string
     */
    public function toJson(FacebookResponse $response)
    {
        return $response->getGraphEdge()->asJson();
    }

    /**
     *
     * @param \Facebook\FacebookResponse $response
     *
     * @return array
     */
    public function toArray(FacebookResponse $response)
    {
        return $response->getGraphEdge()->asArray();
    }

    /**
     * Builds the request
     *
     * @param string $method
     * @param string $endpoint
     * @param string $token
     * @param array  $params
     *
     * @return \Facebook\FacebookResponse
     */
    private function request($method, $endpoint, $token, $params = [])
    {
        $request = new FacebookRequest();
        $request->setAccessToken($token);
        $request->setApp($this->_facebook->getApp());
        $request->setMethod($method);
        $request->setEndpoint($endpoint);
        $request->setParams($params);

        $client = new FacebookClient();

        return $client->sendRequest($request);
    }
}
