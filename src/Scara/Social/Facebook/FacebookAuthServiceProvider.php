<?php

namespace Scara\Social\Facebook;

use Scara\Support\ServiceProvider;
use Scara\Config\Configuration;

/**
 * FacebookAuth Service Provider.
 */
class FacebookAuthServiceProvider extends ServiceProvider
{
    /**
     * Registers the FacebookAuth's service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->create('facebook', function()
        {
            $c = new Configuration();
            $cc = $c->from('social');
            return new FacebookAuth([
                'app_id'        => $cc->get('fb_app_id'),
                'app_secret'    => $cc->get('fb_app_secret'),
            ]);
        });
    }
}
