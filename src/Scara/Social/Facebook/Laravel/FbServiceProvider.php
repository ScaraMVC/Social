<?php

namespace Scara\Social\Facebook\Laravel;

use Illuminate\Support\ServiceProvider;
use Scara\Social\Facebook\FacebookAuth;

class FbServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../../../../../config.php' => config_path('scarasocial.php'),
        ]);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('facebookauth', function($app) {
            return new FacebookAuth([
                'app_id' => config('scarasocial.fb_app_id'),
                'app_secret' => config('scarasocial.fb_app_secret'),
            ]);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['facebookauth'];
    }
}
