<?php

namespace Scara\Social\Facebook\Laravel;

use Illuminate\Support\Facades\Facade;

class FbFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'facebookauth';
    }
}
