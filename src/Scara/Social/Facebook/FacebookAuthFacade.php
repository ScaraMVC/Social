<?php

namespace Scara\Social\Facebook;

use Scara\Support\Facades\Facade;

/**
 * FacebookAuth class's facade.
 */
class FacebookAuthFacade extends Facade
{
    /**
     * Registers the FacebookAuth facade accessor.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'facebook';
    }
}
